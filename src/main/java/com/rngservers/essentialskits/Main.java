package com.rngservers.essentialskits;

import org.bukkit.plugin.java.JavaPlugin;

import com.rngservers.essentialskits.commands.DelayKit;
import com.rngservers.essentialskits.commands.DeleteKit;
import com.rngservers.essentialskits.commands.EditKit;
import com.rngservers.essentialskits.commands.EssentialsKits;
import com.rngservers.essentialskits.commands.KitsGUI;
import com.rngservers.essentialskits.events.Events;
import com.rngservers.essentialskits.kit.KitManager;

public class Main extends JavaPlugin {
	@Override
	public void onEnable() {
		KitManager kits = new KitManager(this);
		saveDefaultConfig();
		kits.createKitsFile();
		kits.getEssentialsItems();
		this.getCommand("essentialskits").setExecutor(new EssentialsKits(this));
		this.getCommand("kitsgui").setExecutor(new KitsGUI(this, kits));
		this.getCommand("editkit").setExecutor(new EditKit(this, kits));
		this.getCommand("delaykit").setExecutor(new DelayKit(this, kits));
		this.getCommand("deletekit").setExecutor(new DeleteKit(this, kits));
		this.getServer().getPluginManager().registerEvents(new Events(this, kits), this);
	}
}