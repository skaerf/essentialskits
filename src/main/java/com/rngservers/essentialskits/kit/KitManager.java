package com.rngservers.essentialskits.kit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.persistence.PersistentDataType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.rngservers.essentialskits.Main;
import com.rngservers.essentialskits.gui.GUI;

public class KitManager {
	private Main plugin;
	private FileConfiguration kits;
	private File kitsFile;
	private JSONObject essentialsItems;

	public KitManager(Main plugin) {
		this.plugin = plugin;
	}

	public void loadKits() {
		try {
			kits.load(kitsFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

	public List<String> getItemStrings(String kitName) {
		List<String> tools = kits.getStringList("kits." + kitName + ".items");
		return tools;
	}

	public List<String> getKitList() {
		List<String> list = new ArrayList<String>();
		if (kits.getConfigurationSection("kits") == null) {
			return null;
		}
		for (String kit : kits.getConfigurationSection("kits").getKeys(false)) {
			list.add(kit);
		}
		return list;
	}

	@SuppressWarnings("deprecation")
	public void openKitList(Player player) {
		// Kit list GUI
		if (!plugin.getConfig().getBoolean("settings.enableList")) {
			player.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY + "]"
					+ ChatColor.RESET + " This is been disabled! Enable it in the config!");
			return;
		}
		loadKits();
		List<ItemStack> itemList = new ArrayList<ItemStack>();
		List<String> kitList = getKitList();
		if (kitList == null) {
			player.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY + "]"
					+ ChatColor.RESET + " No kits found!");
			return;
		}
		// For each ekit
		for (String kit : kitList) {
			// If kit is hidden
			if (plugin.getConfig().getStringList("settings.hideKits").contains(kit)) {
				continue;
			}
			List<String> items = getItemStrings(kit);
			if (!items.isEmpty()) {
				HashMap<Enchantment, Integer> enchants = new HashMap<Enchantment, Integer>();
				String[] part = items.get(0).split(" ");
				String skullOwner = null;
				// If enchants
				for (String testPart : part) {
					if (testPart.contains("player:")) {
						skullOwner = testPart.replace("player:", "");
					}
					HashMap<Enchantment, Integer> enchant = getEnchant(testPart);
					if (enchant != null) {
						enchants.putAll(enchant);
					}
				}
				// Item Material
				Material material = getMaterial(items.get(0).split(" ")[0]);
				if (material == null) {
					continue;
				}
				ItemStack item = new ItemStack(material, 1);
				ItemMeta meta = item.getItemMeta();

				// Display
				String displayConfig = plugin.getConfig().getString("gui.kits." + kit + ".display");
				if (displayConfig == null) {
					displayConfig = plugin.getConfig().getString("gui.display");
				}
				if (meta == null || material.equals(Material.AIR)) {
					plugin.getServer().getLogger()
							.info(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY
									+ "]" + ChatColor.RESET + " Kit " + kit + " has an invalid item: " + material);
					continue;
				}
				meta.setDisplayName(ChatColor.RESET + displayConfig.replace("$name", kit).replace("&", "§"));

				// Lore
				List<String> lores = new ArrayList<String>();
				List<String> loreConfig = plugin.getConfig().getStringList("gui.kits." + kit + ".lore");
				if (loreConfig.isEmpty()) {
					loreConfig = plugin.getConfig().getStringList("gui.lore");
				}
				for (String lore : loreConfig) {
					lores.add(ChatColor.GRAY
							+ lore.replace("$name", kit).replace("$delay", getDelay(kit).toString()).replace("&", "§"));
				}

				if (plugin.getConfig().getBoolean("settings.enchantGlow")) {
					if (!enchants.isEmpty()) {
						meta.addEnchant(Enchantment.DURABILITY, 1, false);
						meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
					}
				}
				if (meta instanceof SkullMeta) {
					SkullMeta skull = (SkullMeta) meta;
					if (skullOwner != null) {
						skull.setOwner(skullOwner);
					}
				}
				meta.setLore(lores);
				item.setItemMeta(meta);
				NamespacedKey key = new NamespacedKey(plugin, "es-kitname");
				item.getItemMeta().getPersistentDataContainer().set(key, PersistentDataType.STRING, kit);
				if (item != null) {
					itemList.add(item);
				}
			}
		}
		if (!itemList.isEmpty()) {
			String guiName = plugin.getConfig().getString("gui.listTitle").replace("&", "§");
			GUI gui = new GUI(this, "§r§r§r§6§r§0§r" + guiName, itemList, 54);
			gui.openInventory(player);
		}
	}

	public Material getMaterial(String item) {
		Material material = Material.getMaterial(item.toUpperCase());
		if (material != null) {
			return material;
		}
		material = Material.getMaterial("LEGACY_" + item.toUpperCase());
		if (material != null) {
			return material;
		}
		if (essentialsItems.get(item) != null) {
			material = Material.getMaterial(essentialsItems.get(item).toString().toUpperCase());
            return material;
		}
		return null;
	}

	public HashMap<Enchantment, Integer> getEnchant(String part) {
		HashMap<Enchantment, Integer> enchants = new HashMap<Enchantment, Integer>();
		@SuppressWarnings("deprecation")
		Enchantment enchantment = Enchantment.getByName(part.split(":")[0].toUpperCase());
		if (enchantment != null) {
			try {
				Integer level = Integer.parseInt(part.split(":")[1]);
				enchants.put(enchantment, level);
			} catch (NumberFormatException e) {
			}
		}
		return enchants;
	}

	@SuppressWarnings("deprecation")
	public String generateItemString(ItemStack item) {
		Material material = item.getType();
		Integer amount = item.getAmount();
		ItemMeta meta = item.getItemMeta();
		String name = meta.getDisplayName();
		List<String> lore = meta.getLore();
		Set<ItemFlag> flags = meta.getItemFlags();
		List<String> flagList = new ArrayList<String>();
		Map<Enchantment, Integer> enchants = meta.getEnchants();
		String fullName = null;
		String fullLore = null;
		String fullSkull = null;
		String fullFlags = null;
		String fullEnchants = null;
		String fullColor = null;

		// If name
		if (meta.hasDisplayName()) {
			fullName = "name:" + name.replace(" ", "_").replace("§", "&");
		}

		// If lore
		if (lore != null) {
			String loreList = String.join("|", lore);
			fullLore = "lore:" + loreList.replace(" ", "_").replace("§", "&");
		}

		// If skull
		if (meta instanceof SkullMeta) {
			SkullMeta skull = (SkullMeta) meta;
			fullSkull = "player:" + skull.getOwner();
		}

		// If flags
		if (flags != null) {
			for (ItemFlag flag : flags) {
				flagList.add(flag.name());
			}
			String joinedFlags = String.join(",", flagList);
			if (!joinedFlags.equals("")) {
				fullFlags = "itemflags:" + joinedFlags;
			}
		}

		// If enchants
		for (Entry<Enchantment, Integer> entry : enchants.entrySet()) {
			fullEnchants = fullEnchants + entry.getKey().getName() + ":" + entry.getValue() + " ";
		}

		// If color
		if (meta instanceof LeatherArmorMeta) {
			LeatherArmorMeta leatherMeta = (LeatherArmorMeta) meta;
			fullColor = "color:" + String.valueOf(leatherMeta.getColor().asRGB());
		}

		// Final string
		String output = material.toString() + " " + amount + " " + fullName + " " + fullLore + " " + fullEnchants + " "
				+ fullFlags + " " + fullColor + " " + fullSkull;
		return StringUtils.normalizeSpace(output);
	}

	@SuppressWarnings("deprecation")
	public ItemStack generateItemStack(String itemString) {
		String[] parts = itemString.split(" ");
		Material material = null;
		Integer amount = null;
		String name = null;
		String[] lores = null;
		List<String> loreList = new ArrayList<String>();
		String[] itemFlags = null;
		HashMap<Enchantment, Integer> enchants = new HashMap<Enchantment, Integer>();
		String color = null;
		String title = null;
		String author = null;
		String bookFile = null;
		String player = null;

		for (String part : parts) {
			// If material
			if (material == null) {
				material = getMaterial(part);
				if (material != null) {
					continue;
				} else {
					plugin.getServer().getLogger().info(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits"
							+ ChatColor.DARK_GRAY + "]" + ChatColor.RESET + " Item " + part + " unrecognised.");
				}
			}

			// If amount
			if (amount == null) {
				try {
					Integer tryAmount = Integer.parseInt(part);
					amount = tryAmount;
					continue;
				} catch (NumberFormatException e) {
				}
			}
			// If name
			if (name == null) {
				if (part.startsWith("name:")) {
					name = part.replace("name:", "").replace("_", " ").replace("&", "§");
					continue;
				}
			}
			if (player == null) {
				if (part.startsWith("player:")) {
					player = part.replace("player:", "").replace("_", " ").replace("&", "§");
					continue;
				}
			}
			// If title
			if (title == null) {
				if (part.startsWith("title:")) {
					title = part.replace("title:", "").replace("_", " ").replace("&", "§");
					continue;
				}
			}
			// If author
			if (author == null) {
				if (part.startsWith("author:")) {
					author = part.replace("author:", "").replace("_", " ").replace("&", "§");
					continue;
				}
			}
			// If book
			if (bookFile == null) {
				if (part.startsWith("book:")) {
					// TODO
				}
			}
			// If lore
			if (lores == null) {
				if (part.startsWith("lore:")) {
					String loreString = part.replace("lore:", "").replace("_", " ").replace("&", "§");
					lores = loreString.split("\\|");
					for (String lore : lores) {
						loreList.add(lore);
					}
					continue;
				}
			}
			// If itemflags
			if (itemFlags == null) {
				if (part.startsWith("itemflags:")) {
					itemFlags = part.replace("itemflags:", "").split(",");
				}
			}
			// If color
			if (color == null) {
				if (part.startsWith("color:")) {
					color = part.replace("color:", "");
				}
			}
			HashMap<Enchantment, Integer> enchant = getEnchant(part);
			if (enchant != null) {
				enchants.putAll(getEnchant(part));
			}
		}
		if (material == null) {
			return null;
		}
		ItemStack item = new ItemStack(material, amount);
		ItemMeta meta = item.getItemMeta();

		if (meta == null || material.equals(Material.AIR)) {
			plugin.getServer().getLogger().info(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits"
					+ ChatColor.DARK_GRAY + "]" + ChatColor.RESET + " Invalid item: " + material);
			return item;
		}

		if (name != null) {
			meta.setDisplayName(name);
		}
		if (lores != null) {
			meta.setLore(loreList);
		}
		if (itemFlags != null) {
			for (String flag : itemFlags) {
				meta.addItemFlags(ItemFlag.valueOf(flag));
			}
		}
		// Leather Armor
		if (meta instanceof LeatherArmorMeta) {
			if (color != null) {
				LeatherArmorMeta leatherMeta = (LeatherArmorMeta) meta;
				try {
					Integer colorInt = Integer.parseInt(color);
					leatherMeta.setColor(Color.fromRGB(colorInt));
				} catch (NumberFormatException e) {
				}
			}
		}
		// Book
		if (meta instanceof BookMeta) {
			BookMeta book = (BookMeta) meta;
			if (title != null) {
				book.setTitle(title);
			}
			if (author != null) {
				book.setAuthor(author);
			}
		}
		// Head
		if (meta instanceof SkullMeta) {
			SkullMeta skull = (SkullMeta) meta;
			if (player != null) {
				skull.setOwner(player);
			}
		}
		item.setItemMeta(meta);
		for (HashMap.Entry<Enchantment, Integer> entry : enchants.entrySet()) {
			item.addUnsafeEnchantment(entry.getKey(), entry.getValue());
		}
		return item;
	}

	public void saveKitsFile() {
		try {
			kits.save(kitsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void removeKit(String kitName) {
		kits.set("kits." + kitName, null);
		reloadKits();
	}

	public void saveKit(String kitName, List<String> items) {
		kits.set("kits." + kitName + ".items", items);
	}

	public Integer getDelay(String kitName) {
		return kits.getInt("kits." + kitName + ".delay");
	}

	public void setDelay(String kitName, Integer delay) {
		kits.set("kits." + kitName + ".delay", delay);
		reloadKits();
	}

	public JSONObject getEssentialsItems() {
		if (essentialsItems == null) {
			JSONParser parser = new JSONParser();
			try {
				FileReader fileReader = new FileReader("plugins/Essentials/items.json");
				Object jsonString = parser.parse(getJSON(fileReader));
				JSONObject json = (JSONObject) jsonString;
				essentialsItems = json;
			} catch (IOException | ParseException e) {
				e.printStackTrace();
				return null;
			}
		}
		return essentialsItems;
	}

	public static String getJSON(FileReader fileReader) {
		try {
			BufferedReader file = new BufferedReader(fileReader);
			StringBuffer input = new StringBuffer();
			String line;
			while ((line = file.readLine()) != null) {
				input.append(line);
				input.append(System.lineSeparator());
			}
			file.close();
			String json = input.toString();
			for (String jsonLine : json.split(System.lineSeparator())) {
				if (jsonLine.startsWith("#")) {
					json = json.replace(jsonLine, "");
				}
			}
			return json;
		} catch (Exception e) {
			return null;
		}
	}

	public void reloadKits() {
		saveKitsFile();
		reloadEssentials();
		loadKits();
	}

	public void reloadEssentials() {
		ConsoleCommandSender console = plugin.getServer().getConsoleSender();
		plugin.getServer().dispatchCommand(console, "essentials reload");
	}

	public void createKitsFile() {
		kitsFile = new File("plugins/Essentials/kits.yml");
		if (!kitsFile.exists()) {
			kitsFile.getParentFile().mkdirs();
			try {
				kitsFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		kits = new YamlConfiguration();
		try {
			kits.load(kitsFile);
			if (!kits.contains("kits")) {
				kits.set("kits", new ArrayList<String>());
				saveKitsFile();
			}
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}
}
