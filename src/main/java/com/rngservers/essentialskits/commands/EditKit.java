package com.rngservers.essentialskits.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.rngservers.essentialskits.Main;
import com.rngservers.essentialskits.gui.GUI;
import com.rngservers.essentialskits.kit.KitManager;

public class EditKit implements CommandExecutor {
	private Main plugin;
	private KitManager kits;

	public EditKit(Main plugin, KitManager kits) {
		this.plugin = plugin;
		this.kits = kits;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (!player.hasPermission("essentialskits.editkit")) {
				player.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY
						+ "]" + ChatColor.RESET + " No Permission!");
				return true;
			}
			if (!plugin.getConfig().getBoolean("settings.enableEditor")) {
				sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY
						+ "]" + ChatColor.RESET + " This has been disabled! Enable it in the config!");
				return true;
			}
			if (args.length < 1) {
				// Kit list
				player.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY
						+ "]" + ChatColor.RESET + " Kits:");
				String kitList = "";
				for (String kit : kits.getKitList()) {
					kitList = kitList + kit + ", ";
				}
				player.sendMessage(kitList);
				sender.sendMessage(ChatColor.GOLD + "NOTE: " + ChatColor.RESET + "Kit names are Case Sensitive!");
			}
			if (args.length == 1) {
				// Save kit
				kits.loadKits();
				List<ItemStack> itemList = new ArrayList<ItemStack>();
				for (String itemString : kits.getItemStrings(args[0])) {
					ItemStack display = kits.generateItemStack(itemString);
					if (display != null) {
						itemList.add(display);
					}
				}
				GUI gui = new GUI(kits, "§r§r§r§6§r§1§r" + args[0] + " Editor", itemList, 36);
				gui.openInventory(player);
			}
		} else {
			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY + "]"
					+ ChatColor.RESET + " This command can only be run by players!");
		}
		return true;
	}
}