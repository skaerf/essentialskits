package com.rngservers.essentialskits.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.rngservers.essentialskits.Main;
import com.rngservers.essentialskits.kit.KitManager;

public class DeleteKit implements CommandExecutor {
	private Main plugin;
	private KitManager kits;

	public DeleteKit(Main plugin, KitManager kits) {
		this.plugin = plugin;
		this.kits = kits;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("essentialskits.deletekit")) {
			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY + "]"
					+ ChatColor.RESET + " No Permission!");
			return true;
		}
		if (!plugin.getConfig().getBoolean("settings.enableEditor")) {
			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY + "]"
					+ ChatColor.RESET + " This has been disabled! Enable it in the config!");
			return true;
		}
		if (args.length < 1) {
			// Kit list
			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY + "]"
					+ ChatColor.RESET + " Kits:");
			String kitList = "";
			for (String kit : kits.getKitList()) {
				kitList = kitList + kit + ", ";
			}
			sender.sendMessage(kitList);
			sender.sendMessage(ChatColor.GOLD + "NOTE: " + ChatColor.RESET + "Kit names are Case Sensitive!");
		}
		if (args.length == 1) {
			// Delete kit
			kits.removeKit(args[0]);
			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY + "]"
					+ " " + ChatColor.GOLD + args[0] + ChatColor.RESET + " kit deleted!");
		}
		return true;
	}
}