package com.rngservers.essentialskits.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.rngservers.essentialskits.Main;
import com.rngservers.essentialskits.kit.KitManager;

public class DelayKit implements CommandExecutor {
	private Main plugin;
	private KitManager kits;

	public DelayKit(Main plugin, KitManager kits) {
		this.plugin = plugin;
		this.kits = kits;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("essentialskits.delaykit")) {
			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY + "]"
					+ ChatColor.RESET + " No Permission!");
			return true;
		}
		if (!plugin.getConfig().getBoolean("settings.enableEditor")) {
			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY + "]"
					+ ChatColor.RESET + " This has been disabled! Enable it in the config!");
			return true;
		}
		if (args.length < 1) {
			// Kit list
			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY + "]"
					+ ChatColor.RESET + " Kits:");
			String kitList = "";
			for (String kit : kits.getKitList()) {
				kitList = kitList + kit + ", ";
			}
			sender.sendMessage(kitList);
			sender.sendMessage(ChatColor.GOLD + "NOTE: " + ChatColor.RESET + "Kit names are Case Sensitive!");
		}
		if (args.length == 1) {
			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY + "]"
					+ " " + ChatColor.GOLD + args[0] + ChatColor.RESET + " delay: " + kits.getDelay(args[0]));
		}
		if (args.length == 2) {
			// Change delay kit
			try {
				Integer delay = Integer.parseInt(args[1]);
				kits.setDelay(args[0], delay);
				sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY
						+ "]" + " " + ChatColor.GOLD + args[0] + ChatColor.RESET + " delay set to " + ChatColor.GOLD
						+ String.valueOf(delay) + ChatColor.RESET + "!");
			} catch (NumberFormatException e) {
				sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY
						+ "]" + ChatColor.RESET + " Invalid, must be an Integer!");
			}
		}
		return true;
	}
}