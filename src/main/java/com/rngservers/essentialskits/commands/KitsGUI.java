package com.rngservers.essentialskits.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.rngservers.essentialskits.Main;
import com.rngservers.essentialskits.kit.KitManager;

public class KitsGUI implements CommandExecutor {
	private Main plugin;
	private KitManager kits;

	public KitsGUI(Main plugin, KitManager kits) {
		this.plugin = plugin;
		this.kits = kits;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (!player.hasPermission("essentialskits.kitsgui") || !player.hasPermission("essentials.kit")) {
				player.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY
						+ "]" + ChatColor.RESET + " No Permission!");
				return true;
			}
			if (!plugin.getConfig().getBoolean("settings.enableList")) {
				sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY
						+ "]" + ChatColor.RESET + " This has been disabled! Enable it in the config!");
				return true;
			}
			if (args.length < 1) {
				// Kit list
				kits.openKitList(player);
			}
			if (args.length == 1) {
				// Save kit
				player.performCommand("essentials:kit " + args[0]);
			}
		} else {
			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "EssentialsKits" + ChatColor.DARK_GRAY + "]"
					+ ChatColor.RESET + " This command can only be run by players!");
		}
		return true;
	}
}