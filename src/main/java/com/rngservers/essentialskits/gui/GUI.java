package com.rngservers.essentialskits.gui;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import com.rngservers.essentialskits.kit.KitManager;

public class GUI implements InventoryHolder {
	private final Inventory inv;
	private List<ItemStack> items;

	public GUI(KitManager kits, String name, List<ItemStack> items, Integer size) {
		this.items = items;
		inv = Bukkit.createInventory(this, size, name);
		initializeKit();
	}

	@Override
	public Inventory getInventory() {
		return inv;
	}

	public void initializeKit() {
		for (ItemStack item : items) {
			inv.addItem(item);
		}
	}

	public void openInventory(Player player) {
		player.openInventory(inv);
	}
}